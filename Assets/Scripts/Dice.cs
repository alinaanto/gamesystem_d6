using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;




public class Dice : MonoBehaviour
{
    public GameObject dice;//empty obj, create from scratch
    //public Sprite dot;
    GameObject dice_Inst;

    public Sprite[] diceDotsSprites = new Sprite[6];
    public SpriteRenderer spriteRenderer;

    private int valueOfDice;//dots
    string name;//TODO if naming is auto ok, then no need for int i in Dice()

    bool animationTrigger;

    Vector2 screenBounds;
    private void Start()
    {
        dice_Inst = new GameObject();

        for(int i = 0;i < 6; i++)
        {
            diceDotsSprites[i] = GetComponent<Sprite>();
        }
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
    }
    public Dice (Color col, int val, int i, Vector2 pos) // for initial set
    {
        dice_Inst = Instantiate(dice, pos, transform.rotation);//TODO create a new scriptable obj instead of intance and load sprites
        dice_Inst.GetComponent<MeshRenderer>().material.color = col;
        Debug.Log(dice_Inst.name);

        animationTrigger = false;

        valueOfDice = val;

        switch (val)
        {

            case 1:
                {
                    dice_Inst.GetComponent<SpriteRenderer>().sprite = diceDotsSprites[0];
                    break;
                }
            case 2:
                {
                    dice_Inst.GetComponent<SpriteRenderer>().sprite = diceDotsSprites[1];
                    break;
                }
            case 3:
                {
                    dice_Inst.GetComponent<SpriteRenderer>().sprite = diceDotsSprites[2];
                    break;
                }
            case 4:
                {
                    dice_Inst.GetComponent<SpriteRenderer>().sprite = diceDotsSprites[3];
                    break;
                }
            case 5:
                {
                    dice_Inst.GetComponent<SpriteRenderer>().sprite = diceDotsSprites[4];
                    break;
                }
            case 6:
                {
                    dice_Inst.GetComponent<SpriteRenderer>().sprite = diceDotsSprites[5];
                    break;
                }
        }
    }



}
