using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceGenerator : MonoBehaviour //creates a dice
{
    public Dice[] DicePool;
    const int arrSize = 10;

    Color[] colorPool;


    void Start()//make sure it's created only once TODO
    {
        colorPool = new Color[5];
        DicePool = new Dice[arrSize];

        colPool();
        CreateDice();
    }

     void colPool()
    {
        for (int i = 0; i < colorPool.Length; i++) //create a pool of colors
        {
            colorPool[i] = new Color(Random.Range(0, 255), Random.Range(0, 255), Random.Range(0, 255));
            Debug.Log(colorPool[i]);
        }
        
    }

     void CreateDice()//initial array
    {

        Vector2 pos = new Vector2(-9, 4);
        float offset = 3;
        for (int i = 0; i < arrSize; i++)
        {
            pos.x += offset;
            if (i == arrSize / 2)
            {
                pos.y += offset;
                pos.x = -9;
            }
            DicePool[i] = new Dice(colorPool[Random.Range(0, 5)],Random.Range(1, 7), i, pos);
        }
    }
    




    void Update()
    {
        
    }
}
